# [Exec] Command Signs
You can place special **[Exec]** signs that will simulate the usage of a command-item by
the player who right-clicked on them.

!!! note
    These signs will run a command on right click.
    
    If you want to **sell** command-items instead, use your favourite shopping or
    kits delivering plugin (for example, [Essentials' _[kit]_-signs](/usage/tutorials/essentials_kits.md)).

## Creating signs
First, [create the command-item](/usage/basics/setting.md) you want to simulate, like usual.
If you already have one, use the [`/cmditem get`](/usage/basics/inspecting.md) command to
retrieve its ID.

Make sure you have the `cmditem.signs.place` permission, then place a sign and fill
it this way:

<code>      [Exec]      </code> « or whatever you set in the [config](/ref/config.md).<br>
<code>    example_id    </code> « the ID of the item to simulate <br>
<code> extra.permission </code> « (**optional**) extra permission node to check before executing.<br>
<code>       $500       </code> « (**optional**) the cost per execution ([requires Vault](/install.md))

If the entry is accepted, the first line should become dark-blue. If not, dark-red.

## Using signs
Players will need the `cmditem.signs.use` permission (granted to everyone by default).

* If an **extra permission node** is specified, it'll be checked against the player too.
* If a **price** is specified, the specified amount will be withdrawn before each execution.

The [_cool-down_](/usage/basics/setting.md#cool-down) period will be specific for each user;
while [_depletion_](/usage/basics/setting.md#depletion), in the case of signs, will never occur.

## Considerations
* If the relative command-item is banned, the sign will stop working too.
* If either _Vault_ or a supported economy plugin aren't installed, signs with a price
  will simply not work.