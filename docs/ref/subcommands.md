These commands are available both to ops and the console:

!!! note
    When used in the console, it's mandatory to provide a command-item ID as the first argument.

* `/cmditem cooldown [<period>]`
> Get or set the [cooldown](/usage/basics/setting.md#cool-down) period for a command item.
>
> **Required permission(s)**: `cmditem.set`

* `/cmditem depletion [<after>]`
> Get or set the [depletion](/usage/basics/setting.md#depletion) value for a command item.
> 
> **Required permission(s)**: `cmditem.set`

* `/cmditem enabled [true|false]`
> [Enable or disable](/usage/advanced/reviewing.md#disabling_command-items) a command-item for everyone.
>
> **Required permission(s)**: `cmditem.set`

* `/cmditem export`
> [Export](/usage/advanced/exporting.md) NBT data from a command-item.
>
> **Required permission(s)**: `cmditem.get`

* `/cmditem get`
> Inspect a command-item.
>
> **Required permission(s)**: `cmditem.get`

* `/cmditem help`
> Obtain help in-game.
>
> **Required permission(s)**: `cmditem`

* `/cmditem set [<id>] [<depletion> [<cooldown>]] [/<command> ...]`<br>
> [Define or configure](/usage/basics/setting.md) command-items.
> 
> **Required permission(s)**: `cmditem.set` (also `cmditem.admin` to assign commands)

* `/cmditem unset`
> [Unbind](/usage/basics/setting.md#unbinding) a command/ID from an item
>
> **Required permission(s)**: `cmditem.set`

* `/cmditem reload`
> Reload the plugin.
>
> **Required permission(s)**: `cmditem.admin`
