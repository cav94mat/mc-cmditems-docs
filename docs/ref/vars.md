These variables are replaced on execution (i.e. when the item is used by a player):

* `${p}`: The name of the player using the item (from now on, the user)
* `${p.x}`: The X coordinate of the user's location.
* `${p.y}`: The Y coordinate of the user's location.
* `${p.z}`: The Z coordinate of the user's location.
* `${p.w}`: The name of the world the user is on.