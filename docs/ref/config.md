The plugin configuration is very simple and self-described, and is entirely contained
in the `<spigot-plugins>/CommandItems/config.yml` file, generated after the first startup.

If you change something here while the server is running, you'll have to manually reload
the plugin with:

```text
/cmditem reload
```

## Settings
The actual configuration section looks like this:

* **default**:
    * **cooldown**: `30`
      > The default cooldown duration for new items.
    * **depletion**: `1`
      > The default depletion value for new items.
* **logging**: `true`
  >If true, the console logs each time a command item is used (for security reasons, this should always be true; but if you're experiencing a lot of flooding on your console output because of this plugin, you may want to disable it)
* **signs**:
    * **enabled**: `true`
      > Whether special [_[Exec]_ signs](/signs/exec.md) are supposed to work in your server.
    * **caption**: `Exec`
      > The caption to be used in the first line of _[Exec]_ signs. <br>
      > Change ONLY if the default value is conflicting with other plugins.
      
* **strings**: [...]
  >Strings displayed to the players/console. You can localize or change them according to your needs.
  
## Data
The following section is automatically filled by the plugin itself, but can be edited by the user:

* **data**:
    * **_&lt;id&gt;_**:
        * **enabled**: `true`
        > Whether the items/signs having this ID can be used.
        * **cooldown**: `30`
        > Cooldown duration for the items/signs having this ID.
        * **depletion**: `5`
        > Maximum amount of uses for the items having this ID.
        * **commands**:
            - `/command arg1 arg2`
            - `/command2 arg1 arg2`
        > Command(s) associated to the items/signs having this ID.
    * ...