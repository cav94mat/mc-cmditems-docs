Just download the latest jar from the Spigot resource page, and put it into your
server's **plugins/** directory.

!!! danger "Security notice"
    If you're running a server where there's multiple levels of staff (most likely, not the case of
    your small home server), make sure to give a look to the [Security Considerations](security.md)
    page before proceeding.

If you plan on using [command signs](/signs/exec.md) as well, in particular the ones with
a price, make sure to also install [Vault 1.5+](https://dev.bukkit.org/projects/vault) and a
compatible economic plugin (e.g. _Essentials_).

The plugin is being developed on 1.11, but should work from at least 1.9 
onwards.
