Since common players, per default configuration, aren't allowed to assign commands to items
(or change them), it's safe to let the items run commands as the console/op.
This way, you may sell items that are able to perform pre-configured, op-level commands and that
will deplete on use.

---

On the other hand, whoever has access to the `cmditem.admin` permission may easily escalate the
whole server permissions system, by crafting items that give them access to commands they're
normally not allowed to run, like for instance:

```plain
/cmditem set rogueItem /minecraft:stop
```

So, it's advisable to grant this permission **ONLY** to top-level administrators, or possibly,
just the server owner.

!!! note
    Ops who have the `cmditem.set` but NOT the `cmditem.admin` are still able to set
    cooldowns and depletion values, and also _clone_ existing command-items into new
    objects.