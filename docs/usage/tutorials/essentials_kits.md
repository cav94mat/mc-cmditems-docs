# Selling command-items via Essentials

As you can [generate command-items via the `/give` command](minecraft_give.md), it's also
possible to sell command-items using Essentials _kits_ (or equivalent plugins, as long as
you're allowed to specify custom NBT data for items).

!!! note
    Unlike [_Exec_ signs](/signs/exec.md), Essentials' _Kit_ signs will simply give one
    or more command items to the user, without executing any command(s).
    
First of all, if you didn't do it already, [create and configure](/basics/setting.md) the
new command-item with `/cmditem set`.

Use [`/cmditem export`](/advanced/exporting.md) to retrieve the NBT data associated to the item.

Finally, just append the exported NBT data to an item in your kit. For instance:

```yaml hl_lines="4"
  examplekit:
    delay: 10
    items:
      - 272 1 {cmd_id:my_example}
```

And place a sign in-game with the following lines:

<code>       [Kit]      </code>
<code>    examplekit    </code> « the ID of the kit <br>
<code>     Everyone     </code> « (**optional**) allows everyone to purchase the kit.<br>
<code>       $500       </code> « (**optional**) the cost of the kit.
