# Generating command-items via /give

Generating command-items via the built-in `/give` (or `/minecraft:give`) command is as easy
as spawning a new item with a custom NBT tag associated to it.

First of all, if you didn't do it already, [create and configure](/basics/setting.md) the
new command-item with `/cmditem set`.

Use [`/cmditem export`](/advanced/exporting.md) to retrieve the NBT data associated to the item.

Finally, just append the exported NBT data to your `/give` call. For instance:

```text
/minecraft:give cav94mat minecraft:redstone 1 0 { cmd_id:"my_example" }
```