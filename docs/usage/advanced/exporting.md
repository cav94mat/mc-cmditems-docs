To print the NBT tag to print the NBT tags, to be used with
[/give](/usage/tutorials/minecraft_give.md) or in
[Essentials' kits](/usage/tutorials/essentials_kits.md), you'll just have to issue:

```text
/cmditem export
```

!!! note
    The output will look similar to this:
    
    <pre><code class="js">{ cmd_id: "item_id_here" }</code></pre>
    
    
    Or to this (if a name and/or _lore_ are attached to the item):
    
    <pre><code class="js">{ cmd_id:"YourID", display:{Name:"Name, if present", Lore:["Lore, if present"] }}</code></pre>

!!! warning
    If the name and/or the lore contain the special formatting char (`§`), all its
    occurrences will be replaced with an ampersand (`&`).
    
To copy the output from the console just open the chat, click on the underlined output data
once, and it'll be mirrored to the chat input.

Now, simply press **CTRL+A** to select the whole line, and eventually, **CTRL+C**.

!!! tip
    Like most commands, you can run `/cmditem export` from the console as long as you
    provide a valid and known ID as it's first argument.