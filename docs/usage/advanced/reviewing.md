By selling or giving (from now on, _distributing_) command-items to people , **copies**
of those specific items are being created in the game.

It's very realistically possible that, soon or later, you'll notice a fault or you'll simply
want to change an option or the command(s) associated to an item you've already started
distributing. Since tracking down each copy of an item would be a hassle, especially on
large servers, the _reviewing_ system has been put in place.

# Why IDs?
At the early development phases of the plugin, the commands and the options (cooldown, depletion)
were directly associated to items via distinct NBT tags.

As soon as the problem described above emerged, a first solution that consisted in associating
an ID to each item (which will remain unchanged in copies) has been put into place. 
This allowed server-admins to "ban" specific IDs whenever some items needed to be re-generated.

Even though this would probably be good enough for small/home servers, it would force a
lot of users who got an item but haven't used it before it was "banned", to discard it.
Depending on the initial cost and/or the server rules, it might not be practicable for the
staff to replace or refund unused and banned items for everyone.

In the current version of the plugin, the command and the options are no longer directly
associated to items via individual NBT tags - they're instead bound to a specific ID, which
is the only thing (along with some statistics, i.e. remaining object life and last usage),
that currently gets attached to the items as NBT tags.

# The actual reviewing process
## In-game
No matter what is the nature of the item you're holding (e.g. the original item was a
_Stone Axe_, but you're now holding an _Emerald_), you can always grab a _copy_ of an
existing item by using the _set_ sub-command and specifying the ID of the item to review:

```text
/cmditem set my_id_here
```

You should now use the [_get_](/usage/basics/inspecting.md) sub-command to confirm you're working
with the right ID.

Then, you might proceed at editing the command and/or the relative options,
like described in the [Creating and editing command-items](/usage/basic/setting.md) page.

To enable or disable all the objects sharing the same ID, just use:

```text
/cmditem enabled true|false
```

Finally, you can destroy or [unset](/usage/basics/setting.md#unbinding) the copy you've
just worked on.

## Via configuration
You can edit the commands and the options associated to the IDs directly in the
[configuration file](ref/config.md), and then reload the plugin configuration in-game (or via
the console/RCON/panel) by issuing:

```text
/cmditem reload
```