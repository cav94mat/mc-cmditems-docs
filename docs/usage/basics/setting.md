To assign a command to an item, you have to assign it an ID as well. This is necessary to
be able to [track-down, edit, or even disable](/usage/advanced/reviewing.md) all the existing
copies of the item once you've distributed it to the players.
 
You just need to provide a new ID to the **set** sub-command, optionally followed by the
command to assign to it:

```text
/cmditem set a_fancy_id /command argument
```

!!! warning
    If you try to specify an ID already in use by another item, the item will be cloned,
    but the command you've supplied will be discarded.
    
    Simply re-issue the command again to override the existing command with a new one.
    The new command will be assigned to **all the items** having the same ID.


# Command
You can change the command (or specify it later) with:

```text
/cmditem set /command ${p} param2 ...
```
!!! note
    You're not required to supply the ID again, if it's already bound.
     
!!! note
    `${p}` is a [variable](/ref/vars.md) that gets replaced with the name of the user/player.

It's possible to assign multiple commands, to be run in chain:

```text
/cmditem set /command ${p} param2 /command2 ${p} param4
```

!!! tip
    If you need to pass an argument starting with a slash (`/`), make sure to escape
    it with a back-slash or it'll be assumed to be a command. For example:
    
    <pre><code class="text">/cmditem set /command ${p} param2 \/param3</code></pre>
    
# Options
## Depletion
You can change how many times the item can be used before it depletes (default is `1`), with:

```text
/cmditem depletion 5
```

!!! tip
    If you set `0` here the item can be used infinite times. ​

## Cool-down

You can also specify a different duration for the cool-down period after each use, in seconds 
(default value is `30`), like this:

```text
/cmditem cooldown 15
```
!!! tip
    If you set `0` here the item won't have a cool-down period.

!!! note
    This option is meaningless if the item is configured to deplete after its first and only
    execution (see above).​
    
# All-in-one alternative
The _set_ sub-command seen above also lets you specify all these options at once.
For example, to set depletion to `3`, you will issue:

```text
/cmditem set 3 /command arg1 arg2 ...
```

To also specify the cool-down duration (e.g. lasting `10` seconds), you'll have to supply it after the depletion value:

```text
/cmditem set 3 10 /command arg1 arg2 ...
```

# Unbinding
You can revert an item to its original state (i.e. detach the command(s) associated), by issuing:

```text
/cmditem unset
```