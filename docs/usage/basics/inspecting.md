You can see all the information relative to the hand-held command-item by issuing:

```text
/cmditem get
```

!!! tip
    Like most commands, you can run `/cmditem get` from the console as long as you
    provide a valid and known ID as it's first argument.