# [Command Items](https://www.spigotmc.org/resources/commanditems.36320/)

This Spigot plugin allows you to assign (op-level) commands to items, that can ultimately be
sold or given to players, and that will deplete after a specified amount of "executions".
The relevant meta-data are stored as NBT tags, so you can generate these items via `/give`
or even sell them as _Essentials_ kits.

Similarly, it's also possible to assign commands to [signs](signs/exec.md).

> For example, you will be able craft a book that grants 5 XP levels to the player who
> uses/executes it (right-click while holding it in the main hand), and that will disappear
> (deplete) just after using it.

!!! warning
    This plugin is in **beta** stage. Please report any bug you find at the
    [dedicated issue-tracker](https://bitbucket.org/cav94mat/mc-cmditems/issues).